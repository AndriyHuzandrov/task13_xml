package com.nexus6.task.comm;

public interface Executable {
  void execute();
}
