package com.nexus6.task.comm;

import com.nexus6.task.utils.TxtUtils;
import java.io.File;
import java.io.IOException;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class ValidateDocComm extends BlancComm {

  public void execute() {
    Schema planesSchema = null;
    String nSpace = XMLConstants.W3C_XML_SCHEMA_NS_URI;
    try {
      Document doc = DocumentBuilderFactory.newInstance()
                                  .newDocumentBuilder().parse(config.getParameter("xmlFile"));
      planesSchema = SchemaFactory.newInstance(nSpace)
                                          .newSchema(new File(config.getParameter("schema")));
      Validator planesValidator = planesSchema.newValidator();
      planesValidator.validate(new DOMSource(doc));
    } catch (SAXException e) {
      TxtUtils.appLog.error(TxtUtils.SCHEMA_ERR);
    } catch (ParserConfigurationException e) {
      TxtUtils.appLog.error(TxtUtils.PARSE_ERR);
    } catch (IOException e) {
      TxtUtils.appLog.error(TxtUtils.FILE_IO_ERR);
    }
  }
}
