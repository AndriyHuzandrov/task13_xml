package com.nexus6.task.comm;

import com.nexus6.task.model.Plane;
import com.nexus6.task.saxhandlers.PlanesParserHandler;
import com.nexus6.task.utils.TxtUtils;
import java.io.File;
import java.io.IOException;
import java.util.function.Supplier;
import java.util.stream.Stream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

public class SAXParseComm extends BlancComm {
  public void execute() {
    try {
     SAXParser planeParser = SAXParserFactory.newInstance().newSAXParser();
     PlanesParserHandler handler = new PlanesParserHandler();
     planeParser.parse(new File(config.getParameter("xmlFile")), handler);
     showPlanes(handler.getPlanes());
    } catch (ParserConfigurationException
            | SAXException
            | IOException e) {
      TxtUtils.appLog.error(TxtUtils.PARSE_ERR);
    }
  }
  private void showPlanes(Supplier<Stream<Plane>> planes) {
    planes
        .get()
        .forEach(System.out::println);
  }
}
