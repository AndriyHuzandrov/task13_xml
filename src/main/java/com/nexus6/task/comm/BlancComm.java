package com.nexus6.task.comm;

import com.nexus6.task.utils.ConfHolder;

abstract class BlancComm implements Executable {
  ConfHolder config;
  BlancComm() {
    config = new ConfHolder();
  }
  abstract public void execute();
}
