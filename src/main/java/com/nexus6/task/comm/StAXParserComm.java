package com.nexus6.task.comm;

import com.nexus6.task.model.Plane;
import com.nexus6.task.model.WeaponConf;
import com.nexus6.task.utils.TxtUtils;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class StAXParserComm extends BlancComm {
  private List<Plane> planes;

  public StAXParserComm() {
    planes = new LinkedList<>();
  }

  public void execute() {
    Plane plane = null;
    try {
      XMLEventReader xmlReader = XMLInputFactory.newInstance()
                  .createXMLEventReader(new FileInputStream(config.getParameter("xmlFile")));
      while(xmlReader.hasNext()) {
        XMLEvent event = xmlReader.nextEvent();
        if(event.isStartElement()) {
          StartElement start = event.asStartElement();
          if(start.getName().getLocalPart().equals("plane")) {
            plane = new Plane();
          } else if(start.getName().getLocalPart().equals("model")) {
            event = xmlReader.nextEvent();
            plane.setModel(event.asCharacters().getData());
          } else if(start.getName().getLocalPart().equals("country")) {
            event  = xmlReader.nextEvent();
            plane.setcOfOrigin(event.asCharacters().getData());
          } else if(start.getName().getLocalPart().equals("weapons")) {
            event = xmlReader.nextEvent();
            plane.setwConf(prepareArmament());
          }
        }
        if(event.isEndElement()) {
          EndElement end = event.asEndElement();
          if(end.getName().getLocalPart().equals("plane")) {
            planes.add(plane);
          }
        }
      }
    showPlaens();
    } catch (FileNotFoundException
            | XMLStreamException e) {
      TxtUtils.appLog.error(TxtUtils.PARSE_ERR);
    }
  }

  private void showPlaens() {
    planes
        .forEach(System.out::println);
  }

  private WeaponConf prepareArmament() {
    WeaponConf wc = new WeaponConf();
    wc.setGunNum(1);
    wc.setMachGunNum(2);
    return wc;
  }
}
