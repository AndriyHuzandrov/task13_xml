package com.nexus6.task.comm;

import com.nexus6.task.utils.TxtUtils;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class HTMLTransofmComm extends BlancComm {
  public void execute() {
    try {
      FileInputStream xml = new FileInputStream(config.getParameter("xmlFile"));
      FileInputStream xsl = new FileInputStream(config.getParameter("xltFile"));
      FileOutputStream out = new FileOutputStream(config.getParameter("outPage"));
      Source xmlDoc =  new StreamSource(xml);
      Source xslDoc =  new StreamSource(xsl);
      Result result =  new StreamResult(out);
      TransformerFactory factory = TransformerFactory.newInstance();
      Transformer trans = factory.newTransformer(xslDoc);
      trans.transform(xmlDoc, result);
    } catch (IOException e) {
      TxtUtils.appLog.error(TxtUtils.FILE_IO_ERR);
    } catch (TransformerException e) {
      TxtUtils.appLog.error(TxtUtils.TRANSF_ERR);
    }
  }
}
