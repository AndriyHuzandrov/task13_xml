package com.nexus6.task.comm;

import com.nexus6.task.model.Plane;
import com.nexus6.task.model.WeaponConf;
import com.nexus6.task.utils.TxtUtils;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DOMParseComm extends BlancComm {
  private List<Plane> planes;

  public DOMParseComm() {
    planes = new LinkedList<>();
  }

  public void execute(){
    Document doc;
    Plane plane;
    try {
      DocumentBuilder domBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      doc = domBuilder.parse(new File(config.getParameter("xmlFile")));
      doc.getDocumentElement().normalize();
      NodeList nodes = doc.getElementsByTagName("plane");
      for(int i = 0; i < nodes.getLength(); i++) {
        Node node = nodes.item(i);
        if(node.getNodeType() == Node.ELEMENT_NODE) {
          Element element = (Element) node;
          plane = prepareObj(element);
          planes.add(plane);
        }
      }
    } catch (ParserConfigurationException
            | SAXException e) {
      TxtUtils.appLog.error(TxtUtils.PARSE_ERR);
    } catch (IOException e) {
      TxtUtils.appLog.error(TxtUtils.FILE_IO_ERR);
    }
    showPlanes();
  }

  private Plane prepareObj(Element e) {
    Plane plane = new Plane();
    int gun;
    int machGun;
    plane.setModel(e.getElementsByTagName("model").item(0).getTextContent());
    plane.setcOfOrigin(e.getElementsByTagName("country").item(0).getTextContent());
    NodeList chars = e.getElementsByTagName("chars");
    plane.setType(((Element)chars.item(0))
                                      .getElementsByTagName("type").item(0).getTextContent());
    plane.setCeiling(Integer.parseInt(((Element)chars.item(0))
                                  .getElementsByTagName("ceiling").item(0).getTextContent()));
    NodeList wp = e.getElementsByTagName("weapons");
    machGun = Integer.parseInt(((Element)wp.item(0))
                                .getElementsByTagName("machinegun").item(0).getTextContent());
    gun = Integer.parseInt(((Element)wp.item(0))
                                      .getElementsByTagName("gun").item(0).getTextContent());
    plane.setwConf(weaponSetUp(gun, machGun));
    plane.setPrice(Integer.parseInt(e.getElementsByTagName("price").item(0).getTextContent()));
    return plane;
  }

  private WeaponConf weaponSetUp(int g, int m) {
    WeaponConf wConf = new WeaponConf();
    wConf.setGunNum(g);
    wConf.setMachGunNum(m);
    return wConf;
  }

  private void showPlanes() {
    planes
        .forEach(System.out::println);
  }
}
