package com.nexus6.task.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TxtUtils {

  public static final Logger appLog = LogManager.getLogger(TxtUtils.class);
  private static List<String> txtMenu;
  public static String CONS_INP_ERROR = "Error reading from console.";
  public static String BAD_MENU_ITM = "Bad menu item";
  public static String SEL_MENU_ITM = "Select menu item: ";
  public static String PARSE_ERR = "Fail to parse xml file";
  public static String TRANSF_ERR = "Transformation error";
  static String PROP_FILE_PATH = "src/main/resources/config.properties";
  public static String PAUSE_MSG = "Press Enter to continue\n";
  static String NO_FILE_ERR = "No such a file";
  public static String FILE_IO_ERR = "File Operation Error";
  public static String SCHEMA_ERR = "Unable to create validation schema";
  public static String ARMAMENT_FORM = "Aircraft is armed with %d machine guns and %d gun%n";
  public static String PLANE_FROM = "Model: %s, aircraft class: %s%n" +
                                    "Produced in: %s%n%sEstimated cost: %d USD%n";
  static {
    txtMenu = new LinkedList<>();
    txtMenu.add("1. Parse XML data utilizing DOM.");
    txtMenu.add("2. Validate XML file with preset schema.");
    txtMenu.add("3. Parse XML data utilizing SAX parser.");
    txtMenu.add("4. Parse XML data utilizing StAX parser.");
    txtMenu.add("5. Transform XML to HTML.");
    txtMenu.add("0. Exit");
  }

  private TxtUtils() {
  }

  public static Supplier<Stream<String>> getTxtMenu() {
    return () -> txtMenu.stream();
  }
}