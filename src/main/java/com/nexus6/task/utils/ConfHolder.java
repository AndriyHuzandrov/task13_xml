package com.nexus6.task.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ConfHolder {
  private Properties projectConf;

  public ConfHolder() {
    projectConf = new Properties();
    try(FileInputStream input = new FileInputStream(TxtUtils.PROP_FILE_PATH)) {
      projectConf.load(input);
    } catch (FileNotFoundException e) {
      TxtUtils.appLog.error(TxtUtils.NO_FILE_ERR);
    } catch (IOException e) {
      TxtUtils.appLog.error(TxtUtils.FILE_IO_ERR);
    }
  }

  public String getParameter(String what) {
    return projectConf.getProperty(what);
  }
}
