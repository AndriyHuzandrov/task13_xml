package com.nexus6.task.saxhandlers;

import com.nexus6.task.model.Plane;
import com.nexus6.task.model.WeaponConf;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class PlanesParserHandler extends DefaultHandler {
  private List<Plane> planes;
  private Plane plane;
  private String data;
  private int g;
  private int mg;
  private boolean isModel;
  private boolean isCountry;
  private boolean isType;
  private boolean isCeiling;
  private boolean isPrice;
  private boolean isMg;
  private boolean isG;

  public PlanesParserHandler() {
    planes = new LinkedList<>();
    isModel = false;
    isCountry = false;
    isType = false;
    isCeiling =false;
    isPrice = false;
    isMg = false;
    isG = false;
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes)
      throws SAXException {
    if(qName.equals("plane")) {
      plane = new Plane();
    } else if(qName.equals("model")) {
      isModel = true;
    } else if(qName.equals("country")) {
      isCountry = true;
    } else if(qName.equals("ceiling")) {
      isCeiling = true;
    } else if(qName.equals("machinegun")) {
      isMg = true;
    } else if(qName.equals("gun")) {
      isG = true;
    }else if(qName.equals("price")) {
      isPrice = true;
    } else if(qName.equals("type")) {
      isType = true;
    }
  }

  public void endElement(String uri, String localName, String qName) throws SAXException {
    if(isModel) {
      plane.setModel(data);
      isModel = false;
    } else if(isCountry) {
      plane.setcOfOrigin(data);
      isCountry = false;
    } else if(isCeiling) {
      plane.setCeiling(Integer.parseInt(data));
      isCeiling = false;
    } else if(isMg) {
      mg = Integer.parseInt(data);
    } else if(isG) {
      g = Integer.parseInt(data);
    } else if(isType) {
      plane.setType(data);
      isType = false;
    } else if(isPrice) {
      plane.setPrice(Integer.parseInt(data));
      isPrice = false;
    }
    if(isMg | isG) {
      plane.setwConf(prepareArmament(mg, g));
      isMg = false;
      isG = false;
    }
    if(qName.equals("plane")) {
      planes.add(plane);
    }
  }

  public void characters(char[] ch, int start, int length) throws SAXException {
    data = new String(ch, start, length);
  }

  public Supplier<Stream<Plane>> getPlanes() {
    return () -> planes.stream();
  }

  private WeaponConf prepareArmament(int mg, int g) {
    WeaponConf wConf = new WeaponConf();
    wConf.setMachGunNum(mg);
    wConf.setGunNum(g);
    return wConf;
  }
}
