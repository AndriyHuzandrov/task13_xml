<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <body>
        <h2 style="color:#c91417" align="center">WW2 Planes</h2>
        <table style="width: 90%; margin-top: 10px; font-size: 0.8em;" border="1px" align="center">
          <tr align="center">
            <th bgcolor="#c91417" style="color:white" rowspan="2">Model</th>
            <th bgcolor="#c91417" style="color:white" rowspan="2">Country of origin</th>
            <th bgcolor="#c91417" style="color:white" rowspan="2">Aircraft type</th>
            <th bgcolor="#c91417" style="color:white" rowspan="2">Ceiling</th>
            <th bgcolor="#c91417" style="color:white" colspan="2">Weapons</th>
            <th bgcolor="#c91417" style="color:white" colspan="3">Dimensions</th>
            <th bgcolor="#c91417" style="color:white" rowspan="2">Price</th>
          </tr>
          <tr align="center">
            <th bgcolor="#c91417" style="color:white">Machine Gun</th>
            <th bgcolor="#c91417" style="color:white">Gun</th>
            <th bgcolor="#c91417" style="color:white">Width</th>
            <th bgcolor="#c91417" style="color:white">Height</th>
            <th bgcolor="#c91417" style="color:white">Length</th>
          </tr>
          <xsl:for-each select="planes/plane">
            <tr align="center">
              <td><xs:value-of select="model"/></td>
              <td><xsl:value-of select="country"/></td>
              <td><xsl:value-of select="chars/type"/></td>
              <td><xsl:value-of select="chars/ceiling"/></td>
              <td><xsl:value-of select="chars/weapons/machinegun"/></td>
              <td><xsl:value-of select="chars/weapons/gun"/></td>
              <td><xsl:value-of select="dimensions/width"/></td>
              <td><xsl:value-of select="dimensions/height"/></td>
              <td><xsl:value-of select="dimensions/length"/></td>
              <td><xsl:value-of select="price"/></td>
            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>